package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
)

//Initially for pworld and like that scripts!
//sreq. minimized
func lastMatch(s string, c string) string {
	stringArr := strings.Split(s, c)
	return stringArr[len(stringArr)-1]
}

func srequest(url string) []byte {
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	} else {
		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Fatal(err)
		} else {
			return body
		}
	}
	return []byte("null")
}

//responseImages to work! function to get last split! continue filename from url
func responseImages(rw http.ResponseWriter, req *http.Request, url string) string {
	fileName := lastMatch(url, "/")
	resp := srequest(url)
	err := ioutil.WriteFile(fileName, resp, 0644)
	if err != nil {
		log.Fatal(err)
	}
	return fileName
}

//minimize
func submitHandler(rw http.ResponseWriter, req *http.Request) {
	rw.Header().Add("Content-type", "text/html")

	var wg sync.WaitGroup
	imgResponses := make(chan string)

	baseUrl := req.FormValue("url")
	init, _ := strconv.Atoi(req.FormValue("init"))
	limit, _ := strconv.Atoi(req.FormValue("limit"))

	for i := init; i <= limit; i++ {
		wg.Add(1)
		iter := strconv.Itoa(i)
		if i < 10 {
			iter = strconv.Itoa(0) + iter
		}
		url := strings.Replace(baseUrl, "$$", iter, -1)
		go func() {
			defer wg.Done()
			imgResponses <- responseImages(rw, req, url)
		}()
	}
	go func() {
		for img := range imgResponses {
			fmt.Fprint(rw, "<img src='"+img+"' alt='"+img+"'/>")
			fmt.Println(img + " Downloaded")
			//INteractive Response js/jquery?
		}
	}()
	wg.Wait()
}

//minimize
func main() {
	http.HandleFunc("/submit", submitHandler)
	http.Handle("/", http.FileServer(http.Dir(".")))
	fmt.Println("Running on :3300")
	err := http.ListenAndServe(":3300", nil)
	if err != nil {
		log.Fatal(err)
	}
}
